//
//  main.c
//  
//  This program runs test cases for two functions : one sorting and another shuffling.
//  Created by Mikhail Yankelevich on 01/05/2018.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "sortAndShuffle.h"
#include "CUnit.h"
#include "Basic.h"

void test_sorting(void);
void test_sorting_wrong(void);
void test_shuffle_Wrong(void);
void test_shuffle(void);

void test_sorting(void)
{
    int array1[3]={10,-3,-2};//initializing arrays to sort correctly
    int array2[7]={13,12,-4,-3,-5,-6,-8};
    int array3[10]={-11,9,-4,-8,-5,-6,0,-10,-12,10};
    int array4[1]={10};
    quickSort(array1,3);//calling a function to each array
    quickSort(array2,7);
    quickSort(array3,10);
    quickSort(array4, 1);
    CU_ASSERT(array1[2]==10&&array1[1]==-2&&array1[0]==-3);//checking different asserts with all possible cases of sorting, all of them are checked if they are sorted
    CU_ASSERT(array2[6]==13&&array2[5]==12&&array2[4]==-3&&array2[3]==-4&&array2[2]==-5&&array2[1]==-6&&array2[0]==-8);
    CU_ASSERT(array3[9]==10&&array3[8]==9&&array3[7]==0&&array3[6]==-4&&array3[5]==-5&&array3[4]==-6&&array3[3]==-8&&array3[2]==-10&&array3[1]==-11&&array3[0]==-12);
    CU_ASSERT(array4[0]=10);//there is only 1 element,so the array is always sorted
}
void test_sorting_wrong(void)
{
    double array1[3]={10.10,-3,-2};//initializing arrays to sort incorrectly
    int array2[7]={13,12,-4,-3,-5,-6,-8};
    int array3[10]={-11,9,-4,-8,-5,-6,0,-10,-12,10};
    int array4[0]={};
    quickSort(array1,3);//calling a function to each array
    quickSort(array2,7);
    quickSort(array3,10);
    quickSort(array4,0);
    CU_ASSERT(array1[2]==10.10&&array1[1]==-2&&array1[0]==-3);//as function works with int array only andthis one is double it can't change anything
    CU_ASSERT(array2[6]==13&&array2[5]==12&&array2[4]==-3&&array2[3]==-5&&array2[2]==-4&&array2[1]==-6&&array2[0]==-8);//this assert is manualy made wrong sorted, so the function gives different results
    CU_ASSERT(array3[9]==10&&array3[8]==9&&array3[7]==0&&array3[6]=='n'&&array3[5]==-5&&array3[4]==-6&&array3[3]==-8&&array3[2]==-10&&array3[1]==-11&&array3[0]==-12);//this assert is manualy made wrong sorted, so the function gives different results
    CU_ASSERT(array4[0]!=array4[0]);//this assert is using an emty array and there is nothing to sort
    
}

void test_shuffle(void)
{
    int a1[5]={0,1,2,3,4};//initializing arrays to shuffle correctly
    int a2[4]={0,-1,2,3};
    int a3[6]={0,1,2,3,-4,-5};
    fisherYates(a1, 5);//calling a function to each array
    fisherYates(a2, 4);
    fisherYates(a3, 6);
    CU_ASSERT(a1[0]!=0&&a1[1]!=1&&a1[2]!=2&&a1[3]!=3&&a1[4]!=4);//for these asserts all elements must be moved from their places
    CU_ASSERT(a2[0]!=0&&a2[1]!=-1&&a2[2]!=2&&a2[3]!=3);
    CU_ASSERT(a3[0]!=0&&a3[1]!=1&&a3[2]!=2&&a3[3]!=3&&a3[4]!=-4&&a3[5]!=-5);
    
}

void test_shuffle_Wrong(void)
{
    int a1[1]={10};//initializing arrays to shuffle incorrectly
    int a2[0]={};
    double a3[6]={0.5,1.5,2.5,3.5,4.5,5.5};
    int a4[5]={0,0,0,0,0};
    int a5[6]={0,1,2,3,-4,-5};
    fisherYates(a1, 1);//calling a function to each array
    fisherYates(a2, 0);
    fisherYates(a3, 6);
    fisherYates(a4, 6);
    fisherYates(a5, 6);
    CU_ASSERT(a1[0]!=10);//array with just one element in the array can't be shuffled as there is nowhere to move the element
    CU_ASSERT(a2[0]!=a2[0]);//array with no elements can not be shuffled
    CU_ASSERT((a3[0]!=0.5&&a3[1]!=1.5&&a3[2]!=2.5&&a3[3]!=3.5&&a3[4]!=4.5&&a3[5]!=5.5));//as function works with int array only andthis one is double it can't change anything
    CU_ASSERT(a4[0]!=0&&a4[1]!=0&&a4[2]!=0&&a4[3]!=0&&a4[4]!=0);//this assert is wrong, because there are same elements in the array and they can't be sorted(nothing will change
    CU_ASSERT(!(a5[0]!=0&&a5[1]!=1&&a5[2]!=2&&a5[3]!=3&&a5[4]!=-4&&a5[5]!=-5));//if any of the valuse stayed on the same place this assert is correct as this is impossible the assert is always wrong
}

int main() {
    CU_initialize_registry();//initializing the registery
    CU_pSuite suite = CU_add_suite("test", 0, 0);///creating a suit
    
    CU_add_test(suite, "Sorting test general ", test_sorting);//adding all tests
    CU_add_test(suite, "Sorting test which should be wrong ", test_sorting_wrong);
    CU_add_test(suite, "Shuffle test which should be wrong ", test_shuffle);
    CU_add_test(suite, "Shuffle test which should be wrong ", test_shuffle_Wrong);
    
    
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();//running tests
    CU_cleanup_registry();//cleaning the registery
    
    return 0;
}
