Name: Mikhail Yankelevich
Student Number: 16205326

I've created 4 test functions:
1) function which checks all correct situations for sorting
2) function which checks all wrong situations for sorting
3) function which checks all correct situations for shuffling
4) function which checks all wrong situations for shuffling

Each function created an array for each case and then ran the function(which ment to be checked) for each array, so all of them are modifyed as they should be after running the function.
Each function had several asserts:
1)  In the 1st function there are several asserts which must be correct for sorting:
    a)In the first function 3 asserts were just checking 3 arrays with different numbers how they should be sorted (manually inputed the right numbers for each element in these array). All of the arrays included in themselves both positive and negative numbers.
    b)The 4th assert was an array of 1 element which should not be changes as the array is always sorted.
2) In the 2nd function there are several asserts which must be wrong for sorting:
    a)the first one passed double to the function and the function can't use any array which is not integer
    b)the second and the tird ones checked situations when arrays are not sorted correctly (deliberate mistake in assert condition)
    d)the forth has checked if zero array will be sorted, as there are no elements it can not be changed in any way
3)The 3d functions is all correct cases for shuffling:
    a)In the first function 3 asserts were just checking 3 arrays with different numbers how they should be shuffled (checking if each number is not equal to the one it used to be). All of the arrays included in themselves both positive and negative numbers.
4)The 4th functions is all wrong cases for shuffling:
    a)the first one uses 1 number array, which can not be shuffled, as there is nothing to change theis number with
    b)the second one uses an empty array and gives an error, as the array with no elements can not be shuffled
    c)the third one passed double to the function and the function can't use any array which is not integer
    d)the forth one passing an array of the same elements and gives an error, because there are same elements in the array and they can't be sorted(nothing will change)
    e)the fifth one is delebiratly made wrong cnd checked if tere is at least 1 element which was't changed, as it is impossible it always going to give an error



